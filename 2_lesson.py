# Напишите пример конкатенации строк c использованием оператора (+)

First = 'Kyrgyzstan '
second = 'moya Rodina'
print(First + second)

# Напишите пример конкатенации строк c использованием F строк

country_name = 'Australia'
location = f'I live in {country_name} for a long time'
print(location)

# Напишите пример конкатенации строк c использованием метода format()

country_name = 'Australia'
homeland = 'Kyrgyzstan'
location_2 = 'I live in {} for a long time, but before have been lived in {}.'.format(country_name, homeland)
print(location_2)

# Напишите пример арифметических вычислений с помощью оператора (+)
a = 15
b = 25
result_1 = a + b
print(result_1)

# Напишите пример арифметических вычислений с помощью оператора (-)
c = 15
d = 25
result_2 = c - d
print(result_2)

# Напишите пример арифметических вычислений с помощью оператора (*)
e = 15
f = 25
result_3 = e * f
print(result_3)

# Напишите пример арифметических вычислений с помощью оператора (/)
j = 15
i = 25
result_4 = j / i
print(result_4)

# Напишите пример арифметических вычислений с помощью оператора (**)
j = 16**9
print(j)

# Напишите пример арифметических вычислений с помощью оператора (%)

o = 100 % 21
print(o)

# Напишите пример арифметических вычислений изменив порядок работы вычислений при помощи скобок
k = (25 - 5) / (30-15)
print(k)
# Напишите пример с неправильными вычислениями использую тип данных Float - дробные числа

t = 0.1 + 0.6 + 0.8
print(t)

# Напишите пример использования метода строк upper()

country_name_1 = 'Australia'
result_9 = country_name_1.upper()
print(result_9)

# Напишите пример использования метода строк lower()

country_name_2 = 'AUSTRALIA'
result_10 = country_name_2.lower()
print(result_10)

# Напишите пример использования метода строк capitalize()
sentence = 'moya Rodina Kyrgyzstan'
result_11 = sentence.capitalize()
print(result_11)

# Прочитайте про метод строк title() - данные нужно найти в интернете

txt_2 = 'Welcome to my world'
x_2 = txt_2.title()
print(x_2)

# Прочитайте про метод строк replace() - данные нужно найти в интернете

txt = 'I like bananas'
x = txt.replace('bananas', 'apples')
print(x)

