# Напишите пример функции возвращающей значения с помощью return
sisters_1 = [{
    'name':'Nuraiym',
    'obrazovanie': 'magistr',
    'language': 'russian',
    },
    {
    'name':'Aigerim',
    'obrazovanie': 'bakalavr',
    'language': 'english',
    },
    {
    'name':'Bekaiym',
    'obrazovanie': 'bakalavr',
    'language': 'turkish',
    }]
sisters_2 = [{
    'name':'Aigul',
    'obrazovanie': 'magistr',
    'language': 'russian',
    },
    {
    'name':'Bema',
    'obrazovanie': 'bakalavr',
    'language': 'english',
    },
    {
    'name':'Elvira',
    'obrazovanie': 'bakalavr',
    'language': 'turkish',
    }]
otobrannye_studenty = []
def IT_kursy(otbor):

    for students in otbor:
        if students['obrazovanie'] == 'bakalavr' :
            otobrannye_studenty.append(students)

    return otobrannye_studenty

result = IT_kursy(sisters_1)
print(result)


print('2 zadanie')
# Напишите пример функции возвращающей значения с помощью return и принимающей в себя любое колличество аргументов

def sum(*args):
    amount = 5

    for slagaemoe in args:
            amount += slagaemoe

    return amount

print(sum(25, 65, 25, 152, 658, 525))

# Напишите пример цикла в цикле
print('3 zadanie')

def sikl_v_sikl(*args):
    otobrannye_studenty_3 = []

    for students in args:
        for sestra in students:
            if sestra['obrazovanie'] == 'bakalavr':
                otobrannye_studenty_3.append(sestra)
    return otobrannye_studenty_3

result = sikl_v_sikl(sisters_1, sisters_2)
print(result)

# Напишите пример клуба с двумя функциями (функция охранник, функция администратор), функция охранника должна принимать в себя любое количество массивов с людьми
print('4 zadanie')

def ohrannik(*args):
    otobrannye_studenty_4 = []

    for students_2 in args:
        for otbor_1 in students_2:
            if otbor_1['language'] == 'russian':
                otobrannye_studenty_4.append(otbor_1)
    return otobrannye_studenty_4

result_2 = ohrannik(sisters_1, sisters_2)
print(result_2)