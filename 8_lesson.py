# - Написать пример записи в файл

file = open('novyi_file.txt', 'w')
file.write('Zapis novogo faila')

# - Написать пример дозаписывания в файл
my_words =  ['nuraiym', 'aigerim', 'bekaiym', 56, 75, 95, 'moon']
my_words = [25, 45, 65, 75]

with open('slova.txt', 'a') as file:
    for word in my_words:
        file.write(f'{word}\n')


# - Написать пример считывания из файла
file = open('slova.txt', 'r')
# print(file.read())
for word in file:
    print(word)

# - Написать пример работы клуба, где функция охранника будет записывать
# в один файл людей прошедших, а в другой людей не прошедших

my_friends = [
    {
        'name': 'Nuraiym',
        'age': 21
    },
    {
        'name': 'Aigerim',
        'age': 22
    },
    {
        'name': 'Bekaiym',
        'age': 17
    }
]

def ohranik(podrugi):
    for podruga in podrugi:
      if podruga['age'] >= 18:
          name = podruga['name']
          age = podruga['age']

          file = open('spisok prowedshih.txt', 'a')
          file.write(f'name:{name}, age: {age}\n')
          file.close()
      else:
          name = podruga['name']
          age = podruga['age']
          file = open('spisok ne prowedwih.txt', 'a')
          file.write(f'name:{name}, age: {age}\n')
          file.close

ohranik(my_friends)



