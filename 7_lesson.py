# - Написать примеры на все методы структуры данных SET, методы взять отсюда
my_set = {2, 5, 8, 10, 45, 78, 85}
my_set_1 = {10, 45, 7, 3, 6}
# print(my_set)
# fSet = frozenset(my_set)
# print(fSet)

# my_set.add(7)
# print(my_set)

# my_set.clear()
# print(my_set)

# my_set_1 = my_set
# my_set_1.add(0)
# print(my_set_1)

# print(my_set.difference(my_set_1))
# print(my_set - my_set_1)

# print(my_set.difference_update(my_set_1))

# my_set.discard(5)
# print(my_set)

# print(my_set.intersection(my_set_1))
# print(my_set & my_set_1)

# print(my_set.intersection_update(my_set))

# print(my_set.isdisjoint(my_set_1))

# print(my_set_1.issubset(my_set))

# print(my_set.pop())
# print(my_set)

# print(my_set.symmetric_difference(my_set_1))

# print(my_set.symmetric_difference_update(my_set_1))

# print(my_set.union(my_set_1))
# print(my_set | my_set_1)

# print(my_set.update(my_set_1))

# - Написать цикл с continue
my_list_1 = ['Nuraiym', 25, 35, 45, 'Bekaiym', 41, 65, 85, 'Aigerim', 105, 115, 125]
for i in my_list_1:
    if i == 'Bekaiym':
        continue
    print(i)

# - Написать цикл с break
my_list = ['Nuraiym', 25, 35, 45, 'Bekaiym', 41, 65, 85, 'Aigerim']
for i in my_list:
    print(i)
    if i == 'Bekaiym':
        break
