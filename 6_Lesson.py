# Напишите пример содержащий функцию принимающую *args
def first(*args):
    print('---args---', args)

first(25, 25, 36, 55)

# Напишите пример содержащий функцию принимающую *kwargs
def second(**kwargs):
    print('---kwargs---', kwargs)

second(a=25, b=25, c=36, d=55)

# Напишите пример содержащий функцию принимающую *args и *kwargs
def third(*args, **kwargs):
    print('---args---', args)
    print('---kwargs---', kwargs)

third(25, 25, 36, 55, a=2, b=5, c=3)

# Напишите пример содержащий функцию принимающую *args и *kwargs а также простые агрументы
def fourth(s, d, f, *args, **kwargs):
    print('--s d f--', s, d, f)
    print('---args---', args)
    print('---kwargs---', kwargs)

fourth(89, 95, 25, 25, 36, 55, a=2, b=5, c=3)

# Напишите пример lambda функции которая возвращает результат умножения числа пришедшего в аргументы на 5
times_5 = lambda num: num * 5
result = times_5(25)
print('--lambda--', result)

# Напишите пример lambda функции которая возвращает результат деления числа пришедшего в аргументы на 5
devided_5 = lambda num: num / 5
result_2 = devided_5(725)
print('--lambda delenie--', result_2)

# Напишите пример lambda функции которая возвращает значение 'age' из пришедшего в аргументы словаря.. ne poluchilos0
sisters = [{
    'name': 'Nuraiym',
    'age': '27',
    },
    {
        'name': "Aigerim",
        'age': 22,
    },
    {
        'anme': 'Bekaiym',
        'age': 19,
    }]

example = lambda sisters: print( 'age')
result_3 = example(sisters)
print(result_3, '--sisters-')

# Напишите пример применения lambda функции в методе list (sort)
my_list_3 = [25, 36, 78, 65, 74]
my_list_3.sort()
print(my_list_3)

# Почитате про python sorted
x = sorted(my_list_3)
print(x)

# Напишите пример применения lambda функции в методе list (sorted)... ne poluchilos
# my_list_4 = [(25, 62, 98), (854, 965, 785), (854, 859, 546)]
# sorted_my_list_4 = sorted(my_list_4, key=lambda x: int[2:])
# print(sorted_my_list_4)

# Напишите пример создания list
my_list = ['nuraiym', 'aigerim', 'bekaiym', 'aigul']

# Напишите чтения из list
print(my_list)

# Напишите пример изменения list
my_list[3] = 'bema'
print(my_list)

# Напишите пример добавления в list
my_list.append('aliya')
print(my_list)

# Напишите пример удаления из list
my_list.pop(4)
print(my_list)

# Напишите пример перевода list в tuple
my_tuple = tuple(my_list)
print(my_tuple)

# Напишите пример создания tuple
my_tuple = (14, 56, 85, 96)

# Напишите чтения из tuple
print(my_tuple)

# Напишите пример перевода tuple в list
my_list_2 = list(my_tuple)
print(my_list_2)

# Напишите пример создания dict
my_dict = {
    'name': 'nuraiym',
    'second name': 'Sardarbekova',
    'job': 'senior manager in Google'
}
# Напишите чтения из dict
print(my_dict)

# Напишите пример изменения dict
my_dict['job'] = ' main manager in Google'
print(my_dict)

# Напишите пример добавления в dict
my_dict.update({'status': True})
print(my_dict)

# Напишите пример удаления из dict
my_dict.pop('second name')
print(my_dict)

# Напишите пример создания set
my_set = {'nuraiym', 'aigerim', 'bekaiym', 'aigul'}
print(my_set)


cars = ['Ford', 'BMW', 'Volvo']

cars.sort(reverse=True)
print(cars)